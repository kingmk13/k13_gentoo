# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="X11 Audio Visualizer for ALSA"
HOMEPAGE="https://github.com/nikp123/xava"
SRC_URI="https://github.com/nikp123/${PN}/archive/${PV}.tar.gz \
	https://gitlab.com/kingmk13/k13-patchs/-/archive/master/k13-patchs-master.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-libs/iniparser"
RDEPEND=""
BDEPEND=""


src_prepare() {
	echo "###########################################"
	echo "################ PREPARE ##################"
	echo "###########################################"
	pwd
	ls -alh
	# Getting the downloaded file in ${WORKDIR}/${P}/
	cp "${WORKDIR}/k13-patchs-master/gentoo-ebuilds/CMakeLists.patch" .
	cat "${WORKDIR}/${P}/CMakeLists.patch"
	echo "- - -> Applying patch..."
	patch -i "${WORKDIR}/${P}/CMakeLists.patch"
	# eapply "${WORKDIR}/${P}/CMakeLists.patch"
	eapply_user
}

src_configure() {
	echo "###########################################"
	echo "############### CONFIGURE #################"
	echo "###########################################"
	pwd
	ls -alh
	# Following instructions at xava's github
	mkdir build
	cd build
	pwd
	cmake .. -DCMAKE_BUILD_TYPE=Release
}

src_compile() {
	echo "###########################################"
	echo "################ COMPILE ##################"
	echo "###########################################"
	pwd
	cd build
	pwd
	ls -alh
	# Make
	emake
	# make
}

src_install() {
	echo "###########################################"
	echo "################ INSTALL ##################"
	echo "###########################################"
	pwd
	cd build
	pwd
	ls -alh
	# Install if Makefile exist
	# if [[ -f Makefile ]] || [[ -f GNUmakefile ]] || [[ -f makefile ]] ; then
	# 	# As the patch didn't get downloaded and applied, and cmake failed , and then make failed, this fail
	# 	emake DESTDIR="${D}" install || die "emake install failed!"
	# fi
	# make DESTDIR="${D}" install || die "make install failed!"
	emake DESTDIR="${D}" install || die "emake install failed!"
}

