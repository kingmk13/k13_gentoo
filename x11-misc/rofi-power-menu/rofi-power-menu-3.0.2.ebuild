# Copyright 2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Provides a Rofi mode for offering basic power menu operations"
HOMEPAGE="https://github.com/jluttine/rofi-power-menu"
SRC_URI="https://github.com/jluttine/${PN}/archive/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="x11-misc/rofi"
RDEPEND=""
BDEPEND=""


src_install() {
	echo "###########################################"
	echo "################ INSTALL ##################"
	echo "###########################################"
	cp rofi-power-menu /usr/local/bin
}

